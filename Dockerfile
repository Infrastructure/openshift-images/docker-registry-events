FROM registry.access.redhat.com/ubi9/python-39:1
ENV ENABLE_MICROPIPENV=1
ENV APP_MODULE=app.main:app

USER 0
ADD --chown=1001:0 . /tmp/src
USER 1001

RUN /usr/libexec/s2i/assemble
CMD uvicorn app.main:app --host 0.0.0.0 --port 8080
