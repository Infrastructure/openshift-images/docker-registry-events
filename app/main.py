import json

import psycopg2
from fastapi import FastAPI, Request
from pydantic import BaseSettings


class Settings(BaseSettings):
    database_url: str = (
        "postgresql://postgres:postgres@postgres:5432/docker-registry-events"
    )


settings = Settings()
postgres = psycopg2.connect(settings.database_url)
app = FastAPI(openapi_url=None, docs_url=None, redoc_url=None)


@app.on_event("startup")
def initialize():
    init_table = """
        CREATE TABLE IF NOT EXISTS events (
            uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
            timestamp TIMESTAMP,
            repository TEXT,
            action TEXT,
            payload TEXT NOT NULL
        )
    """

    with postgres.cursor() as cur:
        cur.execute(init_table)
        postgres.commit()


@app.post("/events")
async def handler(request: Request):
    body = await request.json()
    for event in body.get("events"):
        media_type = event.get("target", {}).get("mediaType")
        repository = event.get("target", {}).get("repository")
        method = event.get("request", {}).get("method")
        action = event.get("action")

        if (
            media_type == "application/vnd.docker.distribution.manifest.v2+json"
            and method in ("GET", "PUT", "POST")
        ):
            with postgres.cursor() as cur:
                cur.execute(
                    "INSERT INTO events (timestamp,  repository, action, payload) VALUES (%s, %s, %s, %s)",
                    (
                        event.get("timestamp"),
                        repository,
                        action,
                        json.dumps(event),
                    ),
                )
                postgres.commit()

    return "OK"
